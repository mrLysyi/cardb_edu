package entity;

import org.junit.Test;

import com.bionic.edu.lysyi.entity.Card;

import org.junit.Assert;

public class CardTets {
	@Test
	public void testValidDepartmentId() throws Exception {
		Card dept = new Card();
		dept.setId(-21488123);
		Assert.assertEquals(-21488123, dept.getId().intValue());
	}

}
