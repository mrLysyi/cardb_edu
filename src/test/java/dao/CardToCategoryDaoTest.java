package dao;

import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.bionic.edu.lysyi.DTO.CardService;
import com.bionic.edu.lysyi.DTO.CardToCategoryService;
import com.bionic.edu.lysyi.entity.Card;
import com.bionic.edu.lysyi.entity.CardToCategory;
import com.bionic.edu.lysyi.entity.Category;

public class CardToCategoryDaoTest {
	private ApplicationContext context;
	private CardToCategoryService ctcService;
	private  int id;
	private Category cat;
	Exception ex = null;

	@Before
	public void setUp() {
		context = new ClassPathXmlApplicationContext("spring/beans.xml");
		ctcService = context.getBean(CardToCategoryService.class);
		createTestData();
	}

	

	@After
	public void after() {

		removeTestData();
	}
	
	@Test
	public void testExist(){
		
		/*System.out.println((ctcService.exist(8)));
		System.out.println(ctcService.exist(0));
		Category cat = new Category();
		cat.setId(1);
		System.out.println("true: " + ctcService.exist(1, cat) );*/
		List<CardToCategory> ctcList = ctcService.selectAll();
		Assert.assertFalse(ctcList.isEmpty());
		this.id = ctcList.get(0).getId();
		Assert.assertTrue(ctcService.exist(id));		//Rewrite
		Assert.assertFalse(ctcService.exist(0));
		//Category c = new Category();
		Assert.assertTrue(ctcService.exist(id, cat));

	}

	private void removeTestData() {
		// TODO Auto-generated method stub
		
	}
	private void createTestData() {
		this.cat = new Category();
		this.cat.setId(1);
		
	}
	
}
