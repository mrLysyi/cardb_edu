package dao;

import java.util.Calendar;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.bionic.edu.lysyi.DTO.CategoryService;

import com.bionic.edu.lysyi.entity.Category;


public class CategoryDaoTest {
	Category tCategory = null;
	private ApplicationContext context;
	private CategoryService catService;
	private int id;
	Exception ex = null;
	
	@Before
	public void setUp() {
		context = new ClassPathXmlApplicationContext("spring/beans.xml");
		catService = context.getBean(CategoryService.class);
		createTestData();
	}

	@After
	public void after() {
		removeTestData();
	}
	
	@Test
	public void testCrud(){
	catService.create(tCategory);
	Assert.assertTrue(catService.exist(tCategory.getCategory()));
	this.id  = catService.getByName(tCategory.getCategory()).getId();
	Assert.assertTrue(catService.exist(id));
	
	tCategory.setCategory("new t category name for test");
	catService.update(tCategory);
	Assert.assertFalse(catService.exist("Test category"));
	Assert.assertTrue(catService.exist("new t category name for test"));
	List<Category> catList =catService.getAll(); 
	Assert.assertTrue(catList.size() > 1 && ((Category)catList.get(0)).getId() > 0);
	/*
	//
	catService.getByName(name);
	catService.findById(id);*/
	
	try {							//check for creation card with same name error (must be false)
		catService.getByName("new t category name for test");
		catService.findById(id);
    } catch (Exception e) {
        ex = e;
    }
	 Assert.assertEquals(null,ex);
	
	catService.delete(id);
		
	}
	
	
	
	private void removeTestData() {
		tCategory = null;
	}
	
	private void createTestData() {
		java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
		tCategory = new Category();
		tCategory.setCategory("Test category");
		tCategory.setCreateDate(date);
		tCategory.setOverview("some overview for test Category");
		
		
	}
}
