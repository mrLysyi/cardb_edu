package dao;

import java.util.Calendar;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.bionic.edu.lysyi.DTO.CardService;
import com.bionic.edu.lysyi.entity.Card;
import com.bionic.edu.lysyi.entity.status.Status;
import com.bionic.edu.lysyi.entity.status.StatusE;

public class StatusDaoTest {
	private ApplicationContext context;
	private CardService cardService;
	private  int id;
	private Card tCard;

	@Before
	public void setUp() {
		context = new ClassPathXmlApplicationContext("spring/beans.xml");
		cardService = context.getBean(CardService.class);
		createTestData();
	}

	@After
	public void after() {

		removeTestData();
	}
	
	@Test
	public void testReadChangeStatus(){
		cardService.create(tCard);
		this.id = cardService.selectByName(tCard.getName()).getId();
		Assert.assertTrue(tCard.getStatus().getName().equals(StatusE.NEW.name()));
		tCard.setStatus(new Status (StatusE.DELETED));
		Assert.assertFalse(tCard.getStatus().getName().equals(StatusE.NEW.name()));
		Assert.assertTrue(tCard.getStatus().getName().equals(StatusE.DELETED.name()));
		cardService.delete(id);

	}
	
private void createTestData() {
		
		tCard = new Card();		
		tCard.setName("testNameStatus");
		tCard.setOverview("testOw S");
		tCard.setSearchTeg("t1 t2 S");
		tCard.setStatus(new Status(StatusE.NEW));
		tCard.setText("test card text");
		
		java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
		tCard.setUpdateDate(date);
		
	}

	private void removeTestData() {
		tCard = null;
	}
}
