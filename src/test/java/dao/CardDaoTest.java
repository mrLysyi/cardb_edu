package dao;

import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.bionic.edu.lysyi.DTO.CardService;
import com.bionic.edu.lysyi.entity.Card;
import com.bionic.edu.lysyi.entity.status.Status;
import com.bionic.edu.lysyi.entity.status.StatusE;
import com.lysyi.dao.exceptions.PreexistingEntityException;

public class CardDaoTest {
	private ApplicationContext context;
	private CardService cardService;
	private  int id;
	private Card tCard;
	Exception ex = null;

	@Before
	public void setUp() {
		context = new ClassPathXmlApplicationContext("spring/beans.xml");
		cardService = context.getBean(CardService.class);
		createTestData();
	}

	@After
	public void after() {

		removeTestData();
	}
	
	@Test
	public void testFindById(){
		Card c = cardService.findById(1);
		Assert.assertNotNull(c);
	}
	
	@Test
	public void testSelectAllCards(){
		List<Card> cardList = cardService.selectAllCards();
		Iterator<Card> it = cardList.iterator();
		System.out.println("list size: " + cardList.size());
		while(it.hasNext()){
			System.out.println("card: " + it.next().toString());
		}
		Assert.assertTrue(cardList.size() > 1);
	}

	@Test
	public void testSelectByName(){
		Assert.assertNotNull(cardService.selectByName("jdk"));
	}
	
	@Test
	public void testCRUD(){		
		cardService.create(tCard);
		this.id = cardService.selectByName(tCard.getName()).getId();
		Assert.assertTrue(cardService.exist(id));
		
		/*try {							//check for creation card with same name error (must be false)
			cardService.create(tCard);
        } catch (org.springframework.transaction.TransactionSystemException e) {
            ex = e;
        }*/
	//	 Assert.assertNotEquals(null,ex);
        
		//---Update
		String oldName = tCard.getName();
		String oldText = tCard.getText();
		tCard.setName("new t name");
		tCard.setText("new t text");
		try {
			cardService.update(tCard);
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		Assert.assertFalse(tCard.getName().equals(oldName));
		Assert.assertFalse(tCard.getText().equals(oldText));
		Assert.assertTrue(tCard.getName().equals("new t name"));
		Assert.assertTrue(tCard.getText().equals("new t text"));
		//-----Delete
		
		cardService.delete(id);
		Assert.assertFalse(cardService.exist(id));
	}
	
	@Test
	public void testCardExist(){
		Assert.assertTrue(cardService.exist(1));
		Assert.assertFalse(cardService.exist(0));
	}
	
	@Test
	public void testSelectCardsByCategory(){
		List<Card> cList = cardService.getCardsByCategory("j_h");
		Assert.assertTrue(cList.size() > 1);
	}
	
	
/*	@Test
	public void testUpdate(){
				
	}
	
	@Test
	public void testDelete(){
		
	}*/
	
	
	
	private void createTestData() {
		
		tCard = new Card();		
		tCard.setName("testName");
		tCard.setOverview("testOw");
		tCard.setSearchTeg("t1 t2");
		tCard.setStatus(new Status(StatusE.NEW));
		tCard.setText("test card text");
		java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
		tCard.setUpdateDate(date);
		
		/*
		 * User user = new User(); user.setName(USER_ID);
		 * user.setPassword(PASSWORD); em.getTransaction().begin();
		 * em.persist(user); em.getTransaction().commit();
		 */
	}

	private void removeTestData() {
		tCard = null;
		/*
		 * em.getTransaction().begin(); User user = em.find(User.class,
		 * USER_ID); if (user != null) { em.remove(user); }
		 * em.getTransaction().commit();
		 */
	}
	
	/*public Card selectByName(String name );	
	public void update(Card card);
	public void create(Card card);
	public void delete(int id);	*/

}
