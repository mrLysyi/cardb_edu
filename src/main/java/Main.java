import java.util.Iterator;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


import com.bionic.edu.lysyi.DTO.CardService;
import com.bionic.edu.lysyi.DTO.StatusService;
import com.bionic.edu.lysyi.entity.Card;
import com.bionic.edu.lysyi.entity.Category;
import com.bionic.edu.lysyi.entity.status.Status;
import com.bionic.edu.lysyi.entity.status.StatusE;

public class Main {
	
	
	
	@SuppressWarnings("resource")
	public static void main(String[] args) {
		
		ApplicationContext context = new ClassPathXmlApplicationContext("/spring/beans.xml");
		
		CardService cardService  = context.getBean(CardService.class);
		StatusService stService = context.getBean(StatusService.class);
		/*List<Card> cardList = cardService.selectAllCards();
		Iterator<Card> it = cardList.iterator();
		System.out.println("list size: " + cardList.size());
		while(it.hasNext()){
			System.out.println("card: " + it.next().toString());
		}*/
		//Status st = stService.getById(1);
		
		//System.out.println(st.getName());
		
		//Card c = cardService.findById(1);
		
		//System.out.println("name: " + c.getStatus().getName() + " id: " + c.getStatus().getId());		
		//System.out.println(cardService.exist(10));
		
	/*	List<Category> catList = (List<Category>) c.getCategoryList();
		System.out.println(catList.size());
		Iterator it = catList.iterator();
		while(it.hasNext()){
			Category cat = (Category) it.next();
			System.out.println(cat.getCategory());
		}*/
		
		List<Card> cardList = cardService.getCardsByCategory("java");
		Iterator<Card> itCat = cardList.iterator();
		System.out.println("list size: " + cardList.size());
		while(itCat.hasNext()){
			System.out.println("card: " + itCat.next().toString());
		}
		
	/*	
		List<Status> sList = stService.getAll();
		Iterator<Status> sIt = sList.iterator();
		System.out.println("list size: " + sList.size());
		while(sIt.hasNext()){
			Status s = sIt.next();
			System.out.println("status: " + "id: " + s.getId()+ " name: " + s.getName()  +" ordinal: " + s.getClass() );
		}*/
		
		
		cardList = cardService.searchCards("as");
		Iterator<Card> itS = cardList.iterator();
		System.out.println("list size: " + cardList.size());
		while(itS.hasNext()){
			System.out.println("card: " + itS.next().toString());
		}
		
		
		//cards without category
		
	/*	cardList = cardService.getCardsNoCat();
		Iterator<Card> itS = cardList.iterator();
		System.out.println("list size: " + cardList.size());
		while(itS.hasNext()){
			Card card = itS.next();
			//if (card.getCategoryList().isEmpty())
			System.out.println("card: " + card.toString());
		}*/
		
		
	}

}
