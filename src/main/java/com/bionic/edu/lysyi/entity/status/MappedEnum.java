package com.bionic.edu.lysyi.entity.status;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(value = RetentionPolicy.RUNTIME)
public @interface MappedEnum {
	Class<? extends Enum> enumClass();
}

//session.createCriteria(Person.class).add(Restrictions.eq("status", Status.NEW)

//session.createCriteria(Person.class).createAlias("status", "st").add(Restrictions.eq("status_id", "id")).add(Restrictions.eq("st.id", "NEW")



//https://habrahabr.ru/post/77982/