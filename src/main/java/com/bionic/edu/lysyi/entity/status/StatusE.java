package com.bionic.edu.lysyi.entity.status;

public enum StatusE {

	NEW("NEW", "new"), 
	ACTIVE("ACT", "Active"), 
	DONE("DONE", "Done"), 
	DELETED("DEL", "Deleted");

	private String id;
	private String description;

	private StatusE(String id, String description) {
		this.id = id;
		this.description = description;
	}

	
	public String getId() {
		return this.id;
	}

	
	public String getDescription() {
		return this.description;
	}
	
	/*NEW,
	ACTIVE, DONE, DELETED;*/

	//public static CustomerStatusType getInstanceById(String id) {...}
	
	

}

/*
 * ('NEW'); ('ACTIVE'); ('DONE'); ('DELETED');
 */