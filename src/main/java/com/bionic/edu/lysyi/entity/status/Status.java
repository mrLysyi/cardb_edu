package com.bionic.edu.lysyi.entity.status;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.bionic.edu.lysyi.entity.BaseEntity;

@Entity
//@Table(name = "STATUS")
@SequenceGenerator(name = "entityIdGenerator", sequenceName = "id")
@MappedEnum(enumClass = StatusE.class)
public class Status extends BaseEntity{	
	
	private static final long serialVersionUID = 1L;


	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Status(){
		super();
	}
	
	public Status(StatusE e){
		this.setId(e.ordinal() + 1); //enum count from 0, instead Derby from 1.
		this.name = e.name();
	}
	private String name;

	public String getName() {
		return name;
	}

	public void setName(StatusE se) {
		this.name = se.name();
	}
	

}
