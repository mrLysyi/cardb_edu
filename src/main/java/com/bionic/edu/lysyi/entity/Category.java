package com.bionic.edu.lysyi.entity;

import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class Category extends BaseEntity{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@NotEmpty(message="Name can not be empty")
	private String category;
	private String overview;
	private java.sql.Date createDate;
	
	@OneToMany(targetEntity=CardToCategory.class, mappedBy="category")
	private Collection<CardToCategory> cardTocategoryes;
	 
	 
	public Collection<CardToCategory> getCardTocategoryes() {
		return cardTocategoryes;
	}
	public void setCardTocategoryes(Collection<CardToCategory> cardTocategoryes) {
		this.cardTocategoryes = cardTocategoryes;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getOverview() {
		return overview;
	}
	public void setOverview(String overview) {
		this.overview = overview;
	}
	public java.sql.Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(java.sql.Date createDate) {
		this.createDate = createDate;
	}
	
	public Category(){
		super();
	}
	

}


/*CREATE TABLE category (
id int not null generated always as identity,	
category varchar(100) not null UNIQUE,
overview varchar(1000) not null,
createDate date not null WITH DEFAULT CURRENT DATE,
 primary key (id)
);*/