package com.bionic.edu.lysyi.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import com.bionic.edu.lysyi.entity.status.Status;

@Entity
@Table(name="CARD")
public class Card extends BaseEntity implements Serializable {	
	
	private static final long serialVersionUID = 1L;
	
	@NotEmpty(message="Name can not be empty")
	private String name;
	
	private String overview;
	
	private String searchTeg;
	
	@Column(insertable=false, updatable=false)
	private java.sql.Date createDate;
	
	//@Temporal(TemporalType.DATE)
	private java.sql.Date updateDate;
	
	@Size(min=2, max=32700, message="wrong size")
	@NotEmpty(message="Main text can not be empty")
	private String text;
	//private int statusId;

	//@Column( nullable = false)
	
	
	@ManyToMany
	@JoinTable(name="cardToCategory", joinColumns=@JoinColumn(name="cardId"),
	   inverseJoinColumns=@JoinColumn(name="categoryId"))
	private List<Category> categoryList; 		//List


	//@OneToOne(fetch=FetchType.LAZY)
	@OneToOne//(mappedBy = "status_fk")
	@JoinColumn(name="statusId")
	private Status status;
	
	
	
	
	
	public List<Category> getCategoryList() {
		return categoryList;
	}

	public void setCategoryList(List<Category> categoryList) {
		this.categoryList = categoryList;
	}

	public Card(){
		super();
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOverview() {
		return overview;
	}
	public void setOverview(String overview) {
		this.overview = overview;
	}
	public String getSearchTeg() {
		return searchTeg;
	}
	public void setSearchTeg(String searchTeg) {
		this.searchTeg = searchTeg;
	}
	public java.sql.Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(java.sql.Date createDate) {
		this.createDate = createDate;
	}
	public java.sql.Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(java.sql.Date updateDate) {
		this.updateDate = updateDate;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus (Status status) {
		this.status = status;
	}
	
/*	public int getStatusId() {
		return statusId;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}*/
	
	
	@Override
	public String toString() {
		return "Card [id=" + super.getId() + ", name=" + name + ", overview=" + overview + ", searchTeg=" + searchTeg
				+ ", createDate=" + createDate + ", updateDate=" + updateDate + ", text=" + text + ", statusId="
				+ status + "category List: " + categoryList.toString() + "]";
	}
	
	

}
