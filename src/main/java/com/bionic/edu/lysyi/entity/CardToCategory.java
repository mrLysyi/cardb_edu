package com.bionic.edu.lysyi.entity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class CardToCategory extends BaseEntity{
	private static final long serialVersionUID = 1L;
	
	private int cardId;
	@ManyToOne
	@JoinColumn(name="categoryId")
	private Category category;
	
	public CardToCategory(){
		super();
	}

	public int getCardId() {
		return cardId;
	}

	public void setCardId(int cardId) {
		this.cardId = cardId;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	
	
}


/*CREATE TABLE cardToCategory (
id int not null generated always as identity,	
cardId int constraint card_fk references card,
categoryId int constraint category_fk references category,
 primary key (id)
);

CREATE TABLE categoryToType(
id int not null generated always as identity,
categoryId int constraint categoryType_fk references category,
typeTId int constraint typeT_fk references typeT,
primary key (id)
);*/