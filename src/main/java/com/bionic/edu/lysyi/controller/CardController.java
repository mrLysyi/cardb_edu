package com.bionic.edu.lysyi.controller;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import javax.inject.Inject;
import javax.validation.Valid;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bionic.edu.lysyi.DTO.CardService;
import com.bionic.edu.lysyi.DTO.CategoryService;
import com.bionic.edu.lysyi.entity.Card;
import com.bionic.edu.lysyi.entity.Category;
import com.bionic.edu.lysyi.entity.status.Status;
import com.bionic.edu.lysyi.entity.status.StatusE;
import com.lysyi.dao.exceptions.PreexistingEntityException;

@Controller
@RequestMapping("/cards")
public class CardController {
	@Inject
	CardService cardService;
	@Inject
	CategoryService categoryService;
	
	List<Card> list = null;

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String showList(ModelMap model) {
		list = cardService.selectAllCards();
		model.addAttribute("cardList", list);
		return "listOfCards";
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String showListV(ModelMap model) {
		list = cardService.selectAllCards();
		model.addAttribute("cardList", list);
		return "listOfCards";
	}
	
	
	
	@RequestMapping(value = "/cardsNoCat", method = RequestMethod.GET)
	public String cardsNoCategory(ModelMap model) {
		List<Card> cardList = cardService.getCardsNoCat();
		model.addAttribute("cardList", cardList);
		return "listOfCards";
	}

	@RequestMapping(value = "/newCard", method = RequestMethod.GET)
	public String newCard(ModelMap model) {
		List<Category> categList = categoryService.getAll();
		model.addAttribute("categorys", categList);
		model.addAttribute("card", new Card());
		return "newCard";
	}

	@RequestMapping(value = "/addCard", method = RequestMethod.POST) //ADD
	public String addCard(@Valid @ModelAttribute("card") Card card, BindingResult bindingResult, ModelMap model) {// @Valid																											
																													
		Status s = new Status(StatusE.NEW);
		java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
		card.setUpdateDate(date);																					// Update Date
		card.setStatus(s); // on Create!!!! err
		List<Category> categList = categoryService.getAll();
		model.addAttribute("categorys", categList);
		model.addAttribute("categorys", categList);
		if (cardService.exist(card.getName()) ) {		//card exist
			System.out.println("11111111");
			bindingResult.rejectValue("name", "cardExist", "card already exist !");
		}
		if (bindingResult.hasErrors()) { // Validation errors or already exist								
			System.out.println("2222" + bindingResult.toString());
			return "newCard"; // Validation
		}

		try {
			cardService.create(card);
		} catch (org.springframework.transaction.TransactionSystemException e) {
			return "newCard"; // send error Pre-existing card
		}

		list = cardService.selectAllCards();
		model.addAttribute("cardList", list);
		return "listOfCards";
	}

	
	@RequestMapping(value = "/editCard", method = RequestMethod.POST) //EDIT
	public String editCard(@Valid @ModelAttribute("card") Card card,  BindingResult bindingResult, ModelMap model)  {// @Valid																													
		
		java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
		card.setUpdateDate(date);																					// Update Date
		
		try {
			cardService.update(card);
		} catch (  org.springframework.transaction.TransactionSystemException e) {
			bindingResult.rejectValue("name", "cardExist", "this name already exist !");
			return "editCard"; // send error Pre-existing card
		} catch (Exception e) {
			System.out.println("EXCEPTION--------------------------"+e.toString());
			bindingResult.rejectValue("name", "cardExist", "unnown error");
			return "editCard"; // send error Pre-existing card
		} 
		list = cardService.selectAllCards();
		
		model.addAttribute("cardList", list);
		return "listOfCards";
	}
	
	
	@RequestMapping(value = "/{cardId}", method = RequestMethod.GET)		//EDIT card
	public String editCard(@PathVariable String cardId, ModelMap model,  RedirectAttributes redir) {
		int id = Integer.valueOf(cardId);
		Card card = cardService.findById(id);
		model.addAttribute("card", card);
		model.addAttribute("statusName", card.getStatus().getName());
		//redir.addFlashAttribute("statusName", card.getStatus().getName());
		return "editCard";
	}
	
	@RequestMapping( value = "/search.form" , method = RequestMethod.GET)
	public String searchCards(@RequestParam("str") String search, ModelMap model){
		
		System.out.println("----------------------------search"+search);
		list = cardService.searchCards(search);
		
		model.addAttribute("cardList", list);
		return "listOfCards";
	}

	@RequestMapping( value = "/cardStatus.form" , method = RequestMethod.POST)//---------------------
	public String changeCardStatus(@RequestParam("status") String status,@RequestParam("cardId") String cardId, ModelMap model){
		Integer id = Integer.parseInt(cardId);
		System.out.println("_________________------------------status: " + status + " id: " + id);
		StatusE inStat = StatusE.valueOf(status);
		Card card = cardService.findById(id);
		card.setStatus(new Status (inStat));
		try {								// update card
			cardService.update(card);
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		list = cardService.selectAllCards();		
		model.addAttribute("cardList", list);
		return "listOfCards";
	}
	
	
	

	@InitBinder
	public void initBinder(WebDataBinder dataBinder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
		dataBinder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}

}
