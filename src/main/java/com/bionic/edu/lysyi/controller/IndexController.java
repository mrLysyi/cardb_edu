package com.bionic.edu.lysyi.controller;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bionic.edu.lysyi.DTO.CardService;
import com.bionic.edu.lysyi.DTO.CategoryService;
import com.bionic.edu.lysyi.entity.Card;
import com.bionic.edu.lysyi.entity.Category;

@Controller
public class IndexController {
	@Inject
	CategoryService catService;
	@Inject
	CardService cardService;
	
	List<Category> list = null;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String showList(ModelMap model) {
		list = catService.getAll();
		model.addAttribute("catList", list);
		return "index";
	}
	
	@RequestMapping(value = "/{catCategory}", method = RequestMethod.GET)		//list of cards by category
	public String editMerchant(@PathVariable String catCategory, ModelMap model) {
		Integer id = Integer.valueOf(catCategory);
		List<Card> cardList = cardService.getCardsByCatId(id);
		model.addAttribute("cardList", cardList);
		return "listOfCards";
		
	}

	
	

}
