package com.bionic.edu.lysyi.controller;

import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bionic.edu.lysyi.DTO.CardService;
import com.bionic.edu.lysyi.DTO.CardToCategoryService;
import com.bionic.edu.lysyi.DTO.CategoryService;
import com.bionic.edu.lysyi.entity.Card;
import com.bionic.edu.lysyi.entity.Category;

@Controller
@RequestMapping("/category")
public class CaregoryController {
	@Inject
	CategoryService categoryService;
	
	@Inject
	CardService cardService;
	
	@Inject
	CardToCategoryService ctc;
	
	List<Category> list = null;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String showList(ModelMap model) {
		List<Category>list = categoryService.getAll();
		model.addAttribute("category", new Category());
		model.addAttribute("categoryList", list);
		return "listOfCategorys";
	}
	

	@RequestMapping(value = "/editCategory", method = RequestMethod.POST) //EDIT
	public String editCard(@Valid @ModelAttribute("category") Category category,  BindingResult bindingResult, ModelMap model)  {																										
		java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
		category.setCreateDate(date);
		try {
			categoryService.update(category);
		} catch (  org.springframework.transaction.TransactionSystemException e) {
			bindingResult.rejectValue("category", "categoryExist", "this name already exist !");
			return "editCategory"; // send error Pre-existing card
		} catch (Exception e) {
			System.out.println("EXCEPTION--------------------------"+e.toString());
			bindingResult.rejectValue("category", "categoryExist", "unnown error");
			return "editCategory"; // send error Pre-existing card
		} 
		List<Category> list = categoryService.getAll();
		
		model.addAttribute("categoryList", list);
		return "listOfCategorys";
	}
	
	@RequestMapping(value = "/{catId}", method = RequestMethod.GET)		//EDIT category
	public String editCategory(@PathVariable String catId, ModelMap model,  RedirectAttributes redir) {
		int id = Integer.valueOf(catId);
		Category category = categoryService.findById(id);
		model.addAttribute("category", category);
		
		return "editCategory";
	}
	
	
	@RequestMapping(value = "/newCategory", method = RequestMethod.POST) //EDIT
	public String newCard(@Valid @ModelAttribute("category") Category category,  BindingResult bindingResult, ModelMap model)  {																										
		java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
		category.setCreateDate(date);
		try {
			categoryService.create(category);
		} catch (  org.springframework.transaction.TransactionSystemException e) {
			bindingResult.rejectValue("category", "categoryExist", "this name already exist !");
			model.addAttribute("categoryList", list);
			return "listOfCategorys"; // send error Pre-existing card
		} catch (Exception e) {
			System.out.println("EXCEPTION--------------------------"+e.toString());
			bindingResult.rejectValue("category", "categoryExist", "unnown error");
			model.addAttribute("categoryList", list);
			return "listOfCategorys"; // send error Pre-existing card
		} 
		 list = categoryService.getAll();
		
		model.addAttribute("categoryList", list);
		return "listOfCategorys";
	}
	
	@RequestMapping(value = "/ctc{cardId}", method = RequestMethod.GET)		//EDIT ctc
	public String editCtc(@PathVariable String cardId, ModelMap model,  RedirectAttributes redir) {
		int id = Integer.valueOf(cardId);
		Card card = cardService.findById(id);
		list = categoryService.getAll();
		model.addAttribute("catL", list);
		model.addAttribute("card", card);
		
		return "editCtc";
	}
	
	@RequestMapping(value = "/del", method = RequestMethod.POST)
	public String delCtc(@RequestParam("cardId") String cardId,@RequestParam("cardName") String cardName,@RequestParam("ctcDel") String ctcDel, ModelMap model ){
		Integer carDidInt = Integer.parseInt(cardId);	//card id
		Integer catId = Integer.parseInt(ctcDel);//categoryId
		System.out.println("-------------" + carDidInt +" "+ cardName + " category name: " +ctcDel);
		
		ctc.delete(carDidInt, catId);
		list = categoryService.getAll();
		model.addAttribute("categoryList", list);
		Card card = cardService.findById(carDidInt);
		model.addAttribute("card", card);	
		list = categoryService.getAll();
		model.addAttribute("catL", list);
		return "editCtc";
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addCtc(@RequestParam("cardId") String cardId,@RequestParam("cardName") String cardName,@RequestParam("ctcAdd") String ctcDel, ModelMap model ){
		Integer carDidInt = Integer.parseInt(cardId);	//card id
		Integer catId = Integer.parseInt(ctcDel);//categoryId
		System.out.println("-------------" + carDidInt +" "+ cardName + " category name: " +ctcDel);
		
		ctc.create(carDidInt, catId);
		list = categoryService.getAll();
		list = categoryService.getAll();
		model.addAttribute("catL", list);
		//model.addAttribute("categoryList", list);
		Card card = cardService.findById(carDidInt);
		model.addAttribute("card", card);		
		return "editCtc";
	}
	
	
	
/*	@RequestMapping(value = "/newCategory", method = RequestMethod.GET)
	public String newCategory(ModelMap model) {
		List<Category> categList = categoryService.getAll();
		model.addAttribute("categorys", categList);
		model.addAttribute("card", new Card());
		return "newCard";
	}*/

}
