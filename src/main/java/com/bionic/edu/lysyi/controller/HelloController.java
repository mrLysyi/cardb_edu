package com.bionic.edu.lysyi.controller;

import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.bionic.edu.lysyi.DTO.CardService;
import com.bionic.edu.lysyi.DTO.StatusService;
import com.bionic.edu.lysyi.entity.Card;
import com.bionic.edu.lysyi.entity.Category;

@Controller
@RequestMapping("/hello")
public class HelloController {
	@Inject
	CardService cardService;

	@RequestMapping(method = RequestMethod.GET)
	public String printHello(ModelMap model) {
		/*ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");

		CardService cardService = context.getBean(CardService.class);
		StatusService stService = context.getBean(StatusService.class);
		String out = null ;
		
		Card c = cardService.findById(1);
		out =  (c.toString()+"<br/>");
		List<Category> catList = (List<Category>) c.getCategoryList();
		out = out + ("categoryes size: " + catList.size() + "<br/> ");
		Iterator it = catList.iterator();
		while(it.hasNext()){
			Category cat = (Category) it.next();
			out= out + ("category: "+ cat.getCategory() +  "<br/> " );
		}	*/
		String out = before();
		
		
		
		model.addAttribute("message", "Hello Spring MVC Framework! <br/> " + "111" +  "<br/>" + out);
		return "hello";
	}

	public String before() {
		/*ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");

		CardService cardService = context.getBean(CardService.class);
		StatusService stService = context.getBean(StatusService.class);
		*/
		
		Card c = cardService.findById(1);
		 StringBuilder out = new StringBuilder(c.toString()+"<br/>");
		//out.append();
		List<Category> catList = (List<Category>) c.getCategoryList();
		out.append("categoryes size: " + catList.size() + "<br/> ");
		Iterator it = catList.iterator();
		while(it.hasNext()){
			Category cat = (Category) it.next();
			out.append("category: "+ cat.getCategory() +  "<br/> " );
		}
		
		return out.toString();//out.toString();
	}

}
