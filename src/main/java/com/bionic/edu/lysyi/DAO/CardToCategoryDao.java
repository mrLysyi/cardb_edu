package com.bionic.edu.lysyi.DAO;

import java.util.List;

import com.bionic.edu.lysyi.entity.CardToCategory;
import com.bionic.edu.lysyi.entity.Category;

public interface CardToCategoryDao {
	public boolean exist(int cardIdS, Category categoryS);
	public boolean exist(int id);
	public void create(CardToCategory ctc);
	public void delete(CardToCategory ctc);
	public void delete (int cardId, int categoryId);
	public void delete (Category category);
	public List<CardToCategory> selectAll();
	public void create(int cardId, int categoryId);

}
