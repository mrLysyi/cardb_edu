package com.bionic.edu.lysyi.DAO;



import java.util.List;

import com.bionic.edu.lysyi.entity.UserT;


public interface UserDao {
	public List<UserT> selectAll();
	public UserT selectById(int id);
	public boolean selectByName(String username);
	public boolean update(UserT user);
	public boolean insert(UserT user);
	public boolean delete(UserT user);
}
