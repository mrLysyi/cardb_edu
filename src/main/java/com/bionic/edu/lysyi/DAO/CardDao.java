package com.bionic.edu.lysyi.DAO;


import java.util.List;


import com.bionic.edu.lysyi.entity.Card;




public interface CardDao {
	public Card findById(int id);
	public boolean exist(int id);
	public boolean exist(String name);
	public List<Card> selectAll();
	public Card selectByName(String name );	
	public void update(Card card);// throws NonexistentEntityException, RollbackFailureException, Exception;
	public void create(Card card);// throws PreexistingEntityException, RollbackFailureException, Exception;
	public void delete(int id);// throws NonexistentEntityException, RollbackFailureException, Exception;
	//public List<SmallCard> getCardCategory();
	public List<Card> getCardsByCategory(String category);
	public List<Card> getCardsByCatId(int id);
	public List<Card> getCardsNoCat();
	public List<Card> searchCards(String search);
	
	//public List<Card> selectBySearchTag(String searchTag);
//-----------------------------
	/*public Customer findById(int id);
	public void save(Customer customer);
	public void remove(int id);
	public void updateCcNo(int id, String ccNo);
	public  LinkedList<Customer> getAll();
	public List<String> getNames(double sumPayed);
	
	public double getPaymentSum();
    public List<Payment> getLargePayments(double limit);
	public Payment findById(int id);
	public List<Result> getTotalReport();
	
	*/
	
	
	
}
