package com.bionic.edu.lysyi.DAO.status;

import org.eclipse.persistence.sessions.Session;


public interface SessionAction {
	void run(Session session);
}
