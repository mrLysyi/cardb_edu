package com.bionic.edu.lysyi.DAO.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import com.bionic.edu.lysyi.DAO.CategoryDao;
import com.bionic.edu.lysyi.entity.Category;
import com.lysyi.dao.exceptions.NonexistentEntityException;
import com.lysyi.dao.exceptions.PreexistingEntityException;

@Repository
public class CategoryDaoImpl implements CategoryDao {
	@PersistenceContext
	private EntityManager em;

	@Override
	public Category findById(int id) {
		Category c = null;
		c = em.find(Category.class, id);
		if (c == null) {
			throw new NullPointerException("null entity query from DB");
		}
		return c;
	}

	@Override
	public Category getByName(String name) {		
		TypedQuery<Category> query = em.createQuery("SELECT c FROM Category c WHERE c.category =:cName", Category.class);
		query.setParameter("cName", name); 
		return query.getSingleResult();
		
	}

	@Override
	public List<Category> getAll() {
		Query query = em.createQuery("SELECT c FROM Category c");
		@SuppressWarnings("unchecked")
		List<Category> catList = query.getResultList();
		return catList;
	}

	@Override
	public boolean exist(int id) {
		Query query = em.createQuery("SELECT COUNT(c.id) FROM Category c WHERE c.id=:catdId");
		query.setParameter("catdId", id);
		Long count = (Long) query.getSingleResult();
		if (count < 1)
			return false;
		return true;
	}

	@Override
	public boolean exist(String name) {
		Query query = em.createQuery("SELECT COUNT(c.category) FROM Category c WHERE c.category=:catdName");
		query.setParameter("catdName", name);
		Long count = (Long) query.getSingleResult();
		if (count < 1)
			return false;
		return true;
	}
	
	@Override
	public void create(Category category) {
		try {
			if(exist(category.getCategory()))			
			throw new PreexistingEntityException("This category already exist");
		} catch (PreexistingEntityException e) {
			e.printStackTrace();
		}
		em.persist(category);
		
	}
	
	@Override
	public void update(Category category) {
		try {
			em.merge(category);
		} catch (Exception ex) {
			try {
				String msg = ex.getLocalizedMessage();
				if (msg == null || msg.length() == 0) {
					Integer id = category.getId();
					if (findById(id) == null) {
						throw new NonexistentEntityException("The category with id " + id + " no longer exists.");
					}
				}
			} catch (NonexistentEntityException e) {
				e.printStackTrace();
			}
		}
		
	}

	

	@Override
	public void delete(int id) {
		try {
			Category cat = null;			
			try {
				cat = em.getReference(Category.class, id); // lazy fetch
			} catch (EntityNotFoundException enfe) {
				throw new NonexistentEntityException("The category with id " + id + " no longer exists.", enfe);
			}
			em.remove(cat);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
