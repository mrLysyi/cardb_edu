package com.bionic.edu.lysyi.DAO.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import com.bionic.edu.lysyi.DAO.StatusDao;
import com.bionic.edu.lysyi.entity.Card;
import com.bionic.edu.lysyi.entity.status.Status;
import com.bionic.edu.lysyi.entity.status.StatusE;

@Repository
public class StatusDaoImpl implements StatusDao {
	@PersistenceContext
	private EntityManager em;



	@Override
	public List<Status> getAll() {
		Query query = em.createQuery("SELECT s FROM Status s");
		@SuppressWarnings("unchecked")
		List<Status> statusList = query.getResultList();
		return statusList;	
		
	}

	@Override
	public Status getById(int id) {
		Status s = null;
		s = em.find(Status.class, id);
		if (s == null) {
			throw new NullPointerException("null entity query from DB");
		}
		return s;
	}

	@Override
	public Status getByName(String name) {
		TypedQuery<Status> query = em.createQuery("SELECT s FROM Status s WHERE s.name =:sName", Status.class);
		query.setParameter("sName", name);
		return query.getSingleResult();
	}

	
	
	
}
