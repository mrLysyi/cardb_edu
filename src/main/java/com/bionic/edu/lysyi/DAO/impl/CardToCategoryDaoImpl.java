package com.bionic.edu.lysyi.DAO.impl;

import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.bionic.edu.lysyi.DAO.CardToCategoryDao;
import com.bionic.edu.lysyi.entity.Card;
import com.bionic.edu.lysyi.entity.CardToCategory;
import com.bionic.edu.lysyi.entity.Category;

/**
 * @author anton
 *
 */
/**
 * @author anton
 *
 */
@Repository
public class CardToCategoryDaoImpl implements CardToCategoryDao {
	@PersistenceContext
	private EntityManager em;


	@Override
	public void create(CardToCategory ctc) {
		
		
	}

	@Override
	public void delete(CardToCategory ctc) {
		// TODO Auto-generated method stub
		
	}

	public CardToCategory find(int cardId, int categoryId){
		Category category = new Category();		
		category.setId(categoryId);
		
		CardToCategory cc = new CardToCategory();	
		Query query = em.createQuery("SELECT ctc FROM CardToCategory ctc WHERE ctc.cardId= :cId AND ctc.category = :cat");
		query.setParameter("cId", cardId);		
		query.setParameter("cat", category);
		@SuppressWarnings("unchecked")
		List<CardToCategory> l =  query.getResultList();
		cc = l.get(0);
		return cc;
	}
	
	@Override
	public void delete (int cardId, int categoryId) {
		/*Category category = new Category();		
		category.setId(categoryId);*/
		
		CardToCategory cc = find(cardId, categoryId);
		em.remove(cc);
		em.flush();
		em.getEntityManagerFactory().getCache().evictAll();
		/*Query query = em.createQuery("DELETE  FROM CardToCategory ctc WHERE ctc.cardId= :cId AND ctc.category = :cat");	
		query.setParameter("cId", cardId);		
		query.setParameter("cat", category).executeUpdate();;*/
		//int count = query.executeUpdate();
		Card card = em.find(Card.class, cardId);		
		em.refresh(card);
		//System.out.println("COUNT del:------------------------------"+count);
	}

	public void create(int cardId, int categoryId){		
		Category category = em.find(Category.class, categoryId);
		CardToCategory ctc = new CardToCategory();
		ctc.setCardId(cardId);
		ctc.setCategory(category);
		em.persist(ctc);
		em.getEntityManagerFactory().getCache().evictAll();	//clean cash
	}
	
	@Override
	public void delete(Category category) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * @param cardIdS - id of CARD entity
	 * @param categoryS - Category entity, must contain ID > 0
	 * @return true, if CardToCategory exist
	 */
	@Override
	public boolean exist(int cardIdS, Category categoryS) {
		Query query = em.createQuery("SELECT COUNT(c.id) FROM CardToCategory c WHERE c.cardId=:varCardId AND c.category=:varCategory");
		query.setParameter("varCardId", cardIdS);
		query.setParameter("varCategory", categoryS);
		Long count = (Long) query.getSingleResult();
		if (count < 1)
			return false;
		return true;
	}

	
	

	@Override
	public boolean exist(int id) {
		Query query = em.createQuery("SELECT COUNT(c.id) FROM CardToCategory c WHERE c.id=:ctcId");
		query.setParameter("ctcId", id);
		Long count = (Long) query.getSingleResult();
		if (count < 1)
			return false;
		return true;
	}

	@Override
	public List<CardToCategory> selectAll() {
		Query query = em.createQuery("SELECT c FROM CardToCategory c ");
		@SuppressWarnings("unchecked")
		List<CardToCategory> ctcList = query.getResultList();
		return ctcList;
		
	}

	/*insert into cardToCategory(cardId, categoryId ) values (1,1);
	delete from cardToCategory where cardId=1 and categoryId=1;*/
	

}
