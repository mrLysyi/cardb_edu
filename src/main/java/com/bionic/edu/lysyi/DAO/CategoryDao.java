package com.bionic.edu.lysyi.DAO;

import java.util.List;

import com.bionic.edu.lysyi.entity.Category;

public interface CategoryDao {
	public Category findById(int id);
	public Category getByName (String name);
	public List<Category> getAll();
	public boolean exist(int id);
	public boolean exist(String name);
	public void update(Category category);
	public void create(Category category);
	public void delete(int id);

}
