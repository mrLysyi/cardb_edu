package com.bionic.edu.lysyi.DAO.status;

import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.List;

import org.eclipse.persistence.sessions.Session;

import com.bionic.edu.lysyi.entity.BaseEntity;
import com.bionic.edu.lysyi.entity.status.MappedEnum;

public class EnumLoader implements SessionAction {
	@Override
	public void run(Session session) {
	/*	Iterator<PersistentClass> mappingList = configuration.getClassMappings();
		while (mappingList.hasNext()) {
			PersistentClass mapping = mappingList.next();

			Class<?> clazz = mapping.getMappedClass();
			if (!BaseEntity.class.isAssignableFrom(clazz))
				continue;
			if (!clazz.isAnnotationPresent(MappedEnum.class))
				continue;

			MappedEnum mappedEnum = clazz.getAnnotation(MappedEnum.class);
			updateEnumIdentifiers(session, mappedEnum.enumClass(), (Class<BaseEntity>) clazz);
		}*/
	}

	private void updateEnumIdentifiers(Session session, Class<? extends Enum> enumClass,
			Class<? extends BaseEntity> entityClass) {
		
		/*List<BaseEntity> valueList = (List<BaseEntity>) session.createCriteria(entityClass).list();

		int maxId = 0;
		Enum[] constants = enumClass.getEnumConstants();
		Iterator<BaseEntity> valueIterator = valueList.iterator();
		while (valueIterator.hasNext()) {
			BaseEntity value = valueIterator.next();

			int valueId = value.getId().intValue();
			setEnumOrdinal(Enum.valueOf(enumClass, value.getCode()), valueId);
			if (valueId > maxId)
				maxId = valueId;
		}

		Object valuesArray = Array.newInstance(enumClass, maxId + 1);
		for (Enum value : constants)
			Array.set(valuesArray, value.ordinal(), value);

		Field field;
		try {
			field = enumClass.getDeclaredField("$VALUES");
			field.setAccessible(true);

			Field modifiersField = Field.class.getDeclaredField("modifiers");
			modifiersField.setAccessible(true);
			modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);

			field.set(null, valuesArray);
		} catch (Exception ex) {
			throw new Exception("Can't update values array: ", ex);
		}*/
	}

	private void setEnumOrdinal(Enum object, int ordinal) throws Exception {
		Field field;
		try {
			field = object.getClass().getSuperclass().getDeclaredField("ordinal");
			field.setAccessible(true);
			field.set(object, ordinal);
		} catch (Exception ex) {
			throw new Exception("Can't update enum ordinal: " + ex);
		}
	}
}