package com.bionic.edu.lysyi.DAO.impl;

// ADD Logger and Exceptions
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import com.bionic.edu.lysyi.DAO.CardDao;
import com.bionic.edu.lysyi.entity.Card;
import com.lysyi.dao.exceptions.NonexistentEntityException;
//import com.lysyi.dao.exceptions.PreexistingEntityException;

/**
 * @author anton
 *
 */
@Repository
public class CardDaoImpl implements CardDao {
	@PersistenceContext
	private EntityManager em;

	@Override
	public Card findById(int id) {
		Card card = null;
		card = em.find(Card.class, id);
		if (card == null) {
			throw new NullPointerException("null entity query from DB");
		}
		return card;
	}

	public boolean exist(int id) {
		Query query = em.createQuery("SELECT COUNT(c.id) FROM Card c WHERE c.id=:cardId");
		query.setParameter("cardId", id);
		Long count = (Long) query.getSingleResult();
		if (count < 1)
			return false;
		return true;
	}

	public boolean exist(String name) {
		Query query = em.createQuery("SELECT COUNT(c.name) FROM Card c WHERE c.name=:cardName");
		query.setParameter("cardName", name);
		Long count = (Long) query.getSingleResult();
		if (count < 1)
			return false;
		return true;
	}

	@Override
	public List<Card> selectAll() {
		Query query = em.createQuery("SELECT c FROM Card c");
		@SuppressWarnings("unchecked")
		List<Card> cardList = query.getResultList();
		return cardList;

	}

	public Card selectByName(String name) {
		TypedQuery<Card> query = em.createQuery("SELECT c FROM Card c WHERE c.name =:cName", Card.class);
		query.setParameter("cName", name);
		return query.getSingleResult();
	}

	@Override
	public void create(Card card) {
		/*try {
			if (exist(card.getName()))
				throw new PreexistingEntityException("This card already exist");
		} catch (PreexistingEntityException e) {
			e.printStackTrace();
		}*/
		//System.out.println("--------------------CARD : "+ card.toString());
		Integer id = (card.getId());
		//System.out.println(id+"------------------------------");
		//if ( id == null || id == 0 )
			em.persist(card);					
			card = selectByName(card.getName());
			em.refresh(card);			
		//else
			//em.merge(card);			
		//em.refresh(card); //refresh for not-updatable fields

	}

	@Override
	public void update(Card card) {
		try {
			em.merge(card);
			card = selectByName(card.getName());
			em.refresh(card);
			//em.refresh(card); //refresh for not-updatable fields
		} catch (Exception ex) {
			try {
				String msg = ex.getLocalizedMessage();
				if (msg == null || msg.length() == 0) {
					Integer id = card.getId();
					if (findById(id) == null) {
						throw new NonexistentEntityException("The card with id " + id + " no longer exists.");
					}
				}
			} catch (NonexistentEntityException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void delete(int id) {
		try {
			Card card = null;
			try {
				card = em.getReference(Card.class, id); // lazy fetch
			} catch (EntityNotFoundException enfe) {
				throw new NonexistentEntityException("The card with id " + id + " no longer exists.", enfe);
			}
			em.remove(card);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// em.find(Card.class, id);

	}

	
	@Override
	public List<Card> getCardsByCategory(String category) { //
		List<Card> list = null;
		String sq = "SELECT c FROM Card c " + "JOIN c.categoryList ctc"
				+ "   " + "where ctc.category= :categoryName ";
		TypedQuery<Card> query = em.createQuery(sq, Card.class);
		query.setParameter("categoryName", category);
		//System.out.println("--------------------------query-------------------" + query.toString());
		list = query.getResultList();
		return list;
	}
	
	@Override
	public List<Card> getCardsByCatId(int id) { //
		List<Card> list = null;
		String sq = "SELECT c FROM Card c " + "JOIN c.categoryList ctc"
				+ "   " + "where ctc.id= :catId ";
		TypedQuery<Card> query = em.createQuery(sq, Card.class);
		query.setParameter("catId", id);
		//System.out.println("--------------------------query-------------------" + query.toString());
		list = query.getResultList();
		return list;
	}
	
	@Override
	public List<Card> getCardsNoCat() { //
		List<Card> list = null;
		String sq = "SELECT c FROM Card c " + " LEFT JOIN c.categoryList ctc"
				+ "   " + "where ctc.id is NULL";
		TypedQuery<Card> query = em.createQuery(sq, Card.class);
		//query.setParameter("catS", "");
		//System.out.println("--------------------------query-------------------" + query.toString());
		list = query.getResultList();
		return list;
	}

	@Override
	public List<Card> searchCards(String search) {
		List<Card> list = null;
		String sq = "SELECT c FROM Card c " + "LEFT JOIN c.categoryList ctc"
				+ "   " + "WHERE c.name LIKE :str  OR c.searchTeg LIKE :str OR c.overview LIKE :str OR   ctc.category LIKE :str ";//   ctc.name LIKE :str  OR
		
		TypedQuery<Card> query = em.createQuery(sq, Card.class);
		query.setParameter("str", search);
		//System.out.println("--------------------------query-------------------" + query.toString());
		list = query.getResultList();
		return list;
	}

	
	
}

/*
"SELECT c FROM Card c WHERE c.name LIKE " + searchTag + "OR c.overview LIKE " + searchTag +
"OR c.searchTeg LIKE " + searchTag + "OR c.category LIKE " + searchTag		);*/



/*
 * "SELECT c FROM Card c " + "(INNER JOIN CardToCategory ctc ON c.id = ctc.cardId "
				+ " INNER JOIN Category cat ON ctc.Category = cat.id) " + "where cat.category= :categoryName ";
 * 
 * 
 * 
 * select * from Card c INNER JOIN CardToCategory ctc on c.id = ctc.cardId INNER
 * JOIN category cat on ctc.categoryId = cat.id where cat.category='j_h';
 * 
 * "SELECT c FROM Card c " + "JOIN  cardToCategory ctc.cardId e"
				+ " JOIN  e.category cat" + "where c.category= :categoryName ";
 * 
 * 
 * 
 * 
 */


