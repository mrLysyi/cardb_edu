package com.bionic.edu.lysyi.DTO.impl;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.transaction.annotation.Transactional;

import com.bionic.edu.lysyi.DAO.CategoryDao;
import com.bionic.edu.lysyi.DTO.CategoryService;
import com.bionic.edu.lysyi.entity.Category;

@Named
public class CategoryServiceImpl implements CategoryService {
	@Inject
	private CategoryDao cDao;
	
	@Override
	public Category findById(int id) {		
		return cDao.findById(id);
	}

	@Override
	public Category getByName(String name) {
		return cDao.getByName(name);
	}

	@Override
	public List<Category> getAll() {
		return cDao.getAll();
	}

	@Override
	public boolean exist(int id) {		
		return cDao.exist(id) ;
	}

	@Transactional
	public void update(Category category) {
		cDao.update(category);

	}

	@Transactional
	public void create(Category category) {
		cDao.create(category);

	}

	@Transactional
	public void delete(int id) {
		cDao.delete(id);

	}

	@Override
	public boolean exist(String name) {
		return cDao.exist(name);
	}

}
