package com.bionic.edu.lysyi.DTO.impl;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.transaction.annotation.Transactional;

import com.bionic.edu.lysyi.DAO.CardDao;
import com.bionic.edu.lysyi.DAO.CategoryDao;
import com.bionic.edu.lysyi.DTO.CardService;
import com.bionic.edu.lysyi.entity.Card;
import com.lysyi.dao.exceptions.NonexistentEntityException;
import com.lysyi.dao.exceptions.PreexistingEntityException;

@Named
public class CardServiceImpl implements CardService {
	@Inject
	private CardDao cardDao;
	@Inject
	private CategoryDao categoryDao;

	@Override
	public Card findById(int id) {
		Card cd  = cardDao.findById(id);
		return cd;
	}
	
	@Override
	public boolean exist(int id) {
		return cardDao.exist(id);
		
	}
	
	@Override
	public boolean exist(String name) {
		return cardDao.exist(name);
		
	}

	@Override
	public List<Card> selectAllCards() {
		List<Card> cl = cardDao.selectAll();
		return cl;
	}

	
		@Override
	public Card selectByName(String name) {
		return cardDao.selectByName(name);
	}

	@Transactional
	public void update(Card card) throws Exception {
		cardDao.update(card);
		
	}

	/**
	 * @
	 */
	@Transactional
	public void create(Card card) {		
			cardDao.create(card);
	}

	@Transactional
	public void delete(int id) {
		cardDao.delete(id);
		
	}

	@Override
	public List<Card> getCardsByCategory(String category) {
		if (!categoryDao.exist(category))
			try {
				throw new NonexistentEntityException("category "+ category + "does not not exist!");
			} catch (NonexistentEntityException e) {
				e.printStackTrace();
			} 
		return cardDao.getCardsByCategory(category);
	}
	
	@Override
	public List<Card> getCardsByCatId(int id) {
		if (!categoryDao.exist(id))
			try {
				throw new NonexistentEntityException("category id "+ id + "does not not exist!");
			} catch (NonexistentEntityException e) {
				e.printStackTrace();
			} 
		return cardDao.getCardsByCatId(id);
	}

	@Override
	public List<Card> getCardsNoCat() {		
		return cardDao.getCardsNoCat();
	}

	@Override
	public List<Card> searchCards(String search) {		
		return cardDao.searchCards(search);
	}



}
