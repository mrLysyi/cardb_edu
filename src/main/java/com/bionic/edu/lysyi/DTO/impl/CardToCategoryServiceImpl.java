package com.bionic.edu.lysyi.DTO.impl;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.transaction.annotation.Transactional;

import com.bionic.edu.lysyi.DAO.CardDao;
import com.bionic.edu.lysyi.DAO.CardToCategoryDao;
import com.bionic.edu.lysyi.DAO.CategoryDao;
import com.bionic.edu.lysyi.DTO.CardToCategoryService;
import com.bionic.edu.lysyi.entity.CardToCategory;
import com.bionic.edu.lysyi.entity.Category;

@Named
public class CardToCategoryServiceImpl implements CardToCategoryService {
	@Inject
	private CardDao cardDao;
	@Inject
	private CategoryDao categoryDao;
	@Inject
	private CardToCategoryDao ctcDao;
	
	
	@Override
	public boolean exist(int cardId, Category cat) {
		return ctcDao.exist(cardId, cat);
	}

	@Override
	public boolean exist(int id) {
		return ctcDao.exist(id);
	}

	@Override
	public void create(CardToCategory ctc) {
		ctcDao.create(ctc);

	}

	@Override
	public void delete(CardToCategory ctc) {
		 ctcDao.delete(ctc);

	}

	@Transactional
	public void delete (int cardId, int categoryId) {
		ctcDao.delete(cardId, categoryId);

	}

	@Override
	public void delete(Category category) {
		ctcDao.delete(category);

	}

	@Override
	public List<CardToCategory> selectAll() {
		return ctcDao.selectAll();
	}

	@Transactional
	public void create(int cardId, int categoryId) {
		 ctcDao.create(cardId,  categoryId);
		
	}

}
