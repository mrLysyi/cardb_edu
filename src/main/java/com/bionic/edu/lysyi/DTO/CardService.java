package com.bionic.edu.lysyi.DTO;

import java.util.List;

import com.bionic.edu.lysyi.entity.Card;
import com.lysyi.dao.exceptions.PreexistingEntityException;

public interface CardService {
	public Card findById(int id);
	public boolean exist(int id);
	public boolean exist(String name);
	public List<Card> selectAllCards();
	public Card selectByName(String name );	
	/**
	 * update card
	 * @param card
	 * @throws PreexistingEntityException 
	 * @throws Exception 
	 */
	public void update(Card card) throws  Exception;
	/**
	 * create or update card
	 * @param card
	 * 
	 */
	public void create(Card card) ;
	public void delete(int id);	
	public List<Card> getCardsByCategory(String category);
	public List<Card> getCardsByCatId(int id);
	public List<Card> getCardsNoCat();
	public List<Card> searchCards(String search);
}
