package com.bionic.edu.lysyi.DTO;

import java.util.List;

import com.bionic.edu.lysyi.entity.CardToCategory;
import com.bionic.edu.lysyi.entity.Category;

public interface CardToCategoryService {
	public boolean exist(int cardId, Category cat);
	public boolean exist(int id);
	public void create(CardToCategory ctc);
	public void delete(CardToCategory ctc);
	public void delete (int cardId, int categoryId);
	public void delete (Category category);
	public List<CardToCategory> selectAll();
	public void create(int cardId, int categoryId);
}
