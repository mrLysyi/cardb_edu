package com.bionic.edu.lysyi.DTO;

import java.util.List;

import com.bionic.edu.lysyi.entity.status.Status;


public interface StatusService {
	public List<Status> getAll();
	public Status getById(int id);
	public Status getByName(String name);

}
