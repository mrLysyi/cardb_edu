package com.bionic.edu.lysyi.DTO.impl;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.bionic.edu.lysyi.DAO.CardDao;
import com.bionic.edu.lysyi.DAO.StatusDao;
import com.bionic.edu.lysyi.DTO.StatusService;
import com.bionic.edu.lysyi.entity.status.Status;
import com.bionic.edu.lysyi.entity.status.StatusE;

@Named
public class StatusServiceImpl implements StatusService {
	@Inject
	private StatusDao sDao;

	@Override
	public List<Status> getAll() {
		
		return sDao.getAll();
	}

	@Override
	public Status getById(int id) {
		
		return sDao.getById(id);
	}

	@Override
	public Status getByName(String name) {
		
	 return sDao.getByName(name);
	}

}
