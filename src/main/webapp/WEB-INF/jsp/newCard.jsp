<%@ page language="java" contentType="text/html;
charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib  prefix="cf" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01
Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>

<link href="<c:url value="/resources/css/style.css" />" 		rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>New Card</title>
<style>
.error {
	color: red;
	font- weight: bold;
}
</style>

</head>

<body>

<div id="mvc">
		<table>
		<tr>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<td><c:url value="/" var="home" /> 
				<a href="/CardB/">home</a><br/>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<td><c:url value="/category" var="Categories" /> 
				<a href="/CardB/category/">Categories</a><br/>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<td><c:url value="/cards/list" var="cList" /> 
				<a href="${cList}"> All cards list</a><br/>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<td> <c:url value="/cards/newCard" var="nCard" />
			<a href="${nCard}">New Card</a><br/>
			<td><c:url value="/hello" var="hello" /> 
				<a href="${hello}">hello</a><br/>
		</tr>
	</table>
	<br/><br/>
		<cf:form method="GET" action="/CardB/cards/search.form">		SEARCH
			<input type="text" name="str">
	     	 <input type="submit">			
		</cf:form>
	<br/>
</div>


	<h2>Card information</h2>
	<cf:form method="POST" action="addCard" modelAttribute="card">
		<%-- <cf:hidden path="id" /> --%>
		<%-- <cf:hidden path="updateDate" /> --%>
		<%-- <cf:hidden path="status" /> --%>
		
		<table>
			<tr>
				<td><cf:label path="name">Name</cf:label></td>
				<td><cf:input path="name" /><br /> <cf:errors path="name"
						cssClass="error" /></td>
			</tr>
			<tr>
				<td><cf:label path="overview" >overview</cf:label></td>
				<td><cf:input path="overview" /></td>
			</tr>
			<tr>
				<td><cf:label path="searchTeg">searchTeg</cf:label></td>
				<td><cf:input path="searchTeg" /></td>
			</tr>
			<tr>
				<td><cf:label path="text">text</cf:label></td>
				<td><cf:input path="text" /><br /> <cf:errors path="text"
						cssClass="error" /></td>
			</tr>

			<tr>
				<td><cf:label path="categoryList[0].id">category</cf:label></td>
				<%-- <td><cf:input path="categoryList[0].id" /><br /> </td> --%>
				<td> <cf:select   path="categoryList[0].id"  >
						<c:forEach var="cat" items="${categorys}">	            		  	
						    <option value="${cat.id}">${cat.category}</option>
						</c:forEach>    
					 </cf:select>
				</td>	
			</tr>

			<tr>
				<td colspan="2"><input type="submit" value="Submit" /></td>
			</tr>
		</table>
	</cf:form>
	
	<h3>Available categories</h3>
	
	<c:forEach var="cat" items="${categorys}">
		<table>
			<tr>
				<td><c:out value="${cat.id}"></c:out></td>
				<td><c:out value="${cat.category}"></c:out></td>		
			<tr>
		</tr>
		</table>
	</c:forEach>

</body>
</html>
