<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Spring MVC - CM</title>
<style>
body {
	margin-top: 20px;
	margin-bottom: 20px;
	background-color: #DFDFDF;
}
</style>
</head>
<body>
	<div style="border: #C1C1C1 solid 1px; background- color: #FCFCFC;">
		<tiles:insertAttribute name="header" />
		<div style="height: 25px; background-color: #FCFCFC;">
			<tiles:insertAttribute name="menu" />
		</div>
		<tiles:insertAttribute name="body" />
		<tiles:insertAttribute name="footer" />
	</div>
</body>
</html>
