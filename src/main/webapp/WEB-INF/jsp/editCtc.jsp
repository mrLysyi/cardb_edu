<%@ page language="java" contentType="text/html; charset=UTF-8" 	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib  prefix="cf" uri="http://www.springframework.org/tags/form" %>


 <html>
	<head>
	
<link href="<c:url value="/resources/css/style.css" />" 		rel="stylesheet" type="text/css">

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Edit ctc</title>
</head>
	<body>	
	
	<h1 align="center">Edit categories and cards</h1>
	<div id="mvc">	
	<table>
		<tr>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<td><c:url value="/" var="home" /> 
				<a href="/CardB/">home</a><br/>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<td><c:url value="/category" var="Categories" /> 
				<a href="/CardB/category/">Categories</a><br/>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<td><c:url value="/cards/list" var="cList" /> 
				<a href="${cList}"> All cards list</a><br/>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<td> <c:url value="/cards/newCard" var="nCard" />
			<a href="${nCard}">New Card</a><br/>
			<td><c:url value="/hello" var="hello" /> 
				<a href="${hello}">hello</a><br/>
		</tr>
	</table>
	<br/><br/>
		<cf:form method="GET" action="/CardB/cards/search.form">		SEARCH
			<input type="text" name="str">
	     	 <input type="submit">			
		</cf:form>
	<br/>	
	
	
	<table>
		<tr>
			<th>Id</th>
			<th>Name</th>
			<th>status</th>
			<th>search Teg</th>
			<th>create Date</th>
			<th>updateDate</th>
			<th>text</th>
			<th>categoryList</th>
			
		</tr>
		
			<tr>
				<td><c:out value="${card.id}" /></td>
				<td><c:out value="${card.name}" /></td>
				<td><c:out value="${card.status.name}" />
				<FORM ACTION="/CardB/cards/cardStatus.form" METHOD="post">   <!-- Change cards status info -->
            		 <select  id="status" name="status" onchange="this.form.submit()" >
            		  <option selected disabled>--</option>
					    <option value="NEW">NEW</option>
					    <option value="ACTIVE">ACTIVE</option>
					    <option value="DONE">DONE</option>
					    <option value="DELETED">DEL</option> <!-- //card id, status id -->
					 </select>
					 <input id="cardId" name="cardId" type="hidden" value="${card.id}"/>
					  <br><br>
           			 <!-- <INPUT  TYPE="submit" VALUE="Submit"> -->
        		</FORM>				
				</td>
				<td><c:out value="${card.searchTeg}" /></td>
				<td><c:out value="${card.createDate}" /></td>
				<td><c:out value="${card.updateDate}" /></td>
				<td><c:out value="${card.text}" /></td>
				<td><c:forEach var="cat" items="${card.categoryList}">
						<c:out value="${cat.category}"></c:out>
					</c:forEach></td>
				<td>	
					
				</td>
			</tr>
				
			
			
			
		

	</table>
				Delete category
					<FORM ACTION="/CardB/category/del" METHOD="post">						
						<input name="cardId" value="${card.id}" type="hidden">
						<input name="cardName" value="${card.name}" type="hidden">
							<select  id="ctcDel" name="ctcDel" onchange="this.form.submit()" >
							 <option selected disabled>--</option>
							<c:forEach var="cat" items="${card.categoryList}">
		            			<option value="${cat.id}">${cat.category}</option>	
							</c:forEach>    						   
						 	</select>
						
					</FORM>	
					
					<br>
					Add category
					<FORM ACTION="/CardB/category/add" METHOD="post">						
						<input name="cardId" value="${card.id}" type="hidden">
						<input name="cardName" value="${card.name}" type="hidden">
							<select  id="ctcAdd" name="ctcAdd" onchange="this.form.submit()" >
							 <option selected disabled>--</option>
							<c:forEach var="c" items="${catL}">
		            			<option value="${c.id}">${c.category}</option>	
							</c:forEach>    						   
						 	</select>
						
					</FORM>	
	
	
	</div>
	
	<br>
	
		
		<p >
		<form style="text-align: center" action="<c:url value="/cards/${card.id}" />" method="GET">
    		<button  type="submit"  title="Back to card"> Back to card</button> 
		</form>
	
	
	
</body>
</html>