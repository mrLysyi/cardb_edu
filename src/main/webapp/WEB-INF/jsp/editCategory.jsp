<%@ page language="java" contentType="text/html;
charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib  prefix="cf" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01
Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<link href="<c:url value="/resources/css/style.css" />" 		rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Edit Card</title>
<style>
.error {
	color: red;
	font- weight: bold;
}
</style>

</head>
<body>
	<div id="mvc">	
	<table>
		<tr>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<td><c:url value="/" var="home" /> 
				<a href="/CardB/">home</a><br/>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<td><c:url value="/category" var="Categories" /> 
				<a href="/CardB/category/">Categories</a><br/>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<td><c:url value="/cards/list" var="cList" /> 
				<a href="${cList}"> All cards list</a><br/>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<td> <c:url value="/cards/newCard" var="nCard" />
			<a href="${nCard}">New Card</a><br/>
			<td><c:url value="/hello" var="hello" /> 
				<a href="${hello}">hello</a><br/>
		</tr>
	</table>
	<br/><br/>
		<cf:form method="GET" action="/CardB/cards/search.form">		SEARCH
			<input type="text" name="str">
	     	 <input type="submit">			
		</cf:form>
	<br/>
 

	<h2 align="center">Edit Category</h2>
	<cf:form method="POST" action="editCategory" modelAttribute="category" >
		<cf:hidden path="id" />
			
		<table>
			<tr>
				<td><cf:label path="category">Category</cf:label></td>
				<td><cf:input path="category" /><br /> <cf:errors path="category"
						cssClass="error" /></td>
			</tr>
			<tr>
				<td><cf:label path="overview">overview</cf:label></td>
				<td><cf:input path="overview" /></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" value="Submit" /></td>
			</tr>
			
			</table>
			
				
					
			
		</cf:form>
	</div>

</body>
</html>