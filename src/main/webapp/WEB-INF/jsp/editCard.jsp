<%@ page language="java" contentType="text/html;
charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib  prefix="cf" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01
Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<link href="<c:url value="/resources/css/style.css" />" 		rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Edit Card</title>
<style>
.error {
	color: red;
	font- weight: bold;
}
</style>

</head>
<body>

	<table>
		<tr>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<td><c:url value="/" var="home" /> 
				<a href="/CardB/">home</a><br/>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<td><c:url value="/hello" var="hello" /> 
				<a href="${hello}">hello</a><br/>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<td><c:url value="/cards/list" var="cList" /> 
				<a href="${cList}"> All cards list</a><br/>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<td> <c:url value="/cards/newCard" var="nCard" />
			<a href="${nCard}">New Card</a><br/>
		</tr>
	</table>
	<br/><br/>
<div id="mvc">

 <%-- <cf:hidden path="statusName"  />  --%>

	<h2>Edit Card</h2>
	<cf:form method="POST" action="editCard" modelAttribute="card" >
		<cf:hidden path="id" />
		<%-- <cf:hidden path="updateDate" /> --%>			
		  <cf:hidden path="status" /> 	 	 
		
		<table>
			<tr>
				<td><cf:label path="name">Name</cf:label></td>
				<td><cf:input path="name" /><br /> <cf:errors path="name"
						cssClass="error" /></td>
			</tr>
			<tr>
				<td><cf:label path="overview">overview</cf:label></td>
				<td><cf:input path="overview" /></td>
			</tr>
			<tr>
				<td><cf:label path="searchTeg">searchTeg</cf:label></td>
				<td><cf:input path="searchTeg" /></td>
			</tr>
			<tr>
				<td><cf:label path="text">text</cf:label></td>
				<td><cf:input path="text" /><br /> <cf:errors path="text"
						cssClass="error" /></td>
			</tr>
			<tr>
				<%-- <<c:out value="${card.status}" /> --%>
				<td><cf:label path="status">Status: </cf:label></td>
				<td>${card.status.name}</td>
					<td> <cf:select   path="status.id"  >
	            		  <option  value= "${card.status.id}" selected="selected">--</option>
						    <option value="1">NEW</option>
						    <option value="2">ACTIVE</option>
						    <option value="3">DONE</option>
						    <option value="4">DEL</option> <!-- //card id, status id -->
						 </cf:select>
					</td>				
				</tr> 
				
				 <%-- <cf:hidden path="status.id"  /> --%>
				 <%--  <cf:hidden path="status.name"  /> --%> 
			<tr>
				<%-- <td><c:out value="${card.status.name}" /></td> --%>
				<c:forEach var="cat" items="${card.categoryList}" varStatus="theCount">		<!-- checkbox -->
					<cf:hidden path="categoryList[${theCount.index}].id" value="${cat.id}" />
					<cf:checkbox path="categoryList[${theCount.index}].id"  value="3"  label="${cat.category}" /> 					
           			 <c:if test="${status.value}">checked</c:if>
					<%-- <input type="checkbox" id="cbox2"  value="${cat.id}"> <label for="cbox2 ">This is the second checkbox</label> --%>
					
	   			</c:forEach>	
				
				
			<tr>
				<td colspan="2"><input type="submit" value="Submit" /></td>
			</tr>
			</table>
			
					<h3>categories</h3>
					<c:forEach var="cat" items="${card.categoryList}" varStatus="theCount">
						<%-- <cf:hidden path="categoryList[${theCount.index}].id" value="${cat.id}" />  --%>  <!-- Hidden values for category id -->
						
						<table align="left">
							<tr>
								<td><c:out value="${cat.id}"></c:out></td> </tr>
							<tr><td><c:out value="${cat.category}"></c:out></td>
							</tr>
						</table>
					</c:forEach>
			
		</cf:form>
		<br>
		
		<p >
		<form style="text-align: center" action="<c:url value="/category/ctc${card.id}" />" method="GET">
    		<button  type="submit"  title="Edit card categories"> Edit card categories</button> 
		</form>
		
		<br>
		
		
		<form style="text-align: center" action="<c:url value="/cards/list" />" method="GET">
    		<button  type="submit"  title="LIST all Cards"> Edit card categories</button> 
		</form>
		
		
		
	</div>

</body>
</html>