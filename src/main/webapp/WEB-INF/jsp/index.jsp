<%@ page language="java" contentType="text/html; charset=UTF-8" 	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib  prefix="cf" uri="http://www.springframework.org/tags/form" %>


 <html>
	<head>
	
<link href="<c:url value="/resources/css/style.css" />" 		rel="stylesheet" type="text/css">

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Start page</title>
</head>
	<body>	
	
	<h1 align="center">CARD IDEA main page</h1>
	<div id="mvc">	
	<table>
		<tr>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<td><c:url value="/" var="home" /> 
				<a href="/CardB/">home</a><br/>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<td><c:url value="/category" var="Categories" /> 
				<a href="/CardB/category/">Categories</a><br/>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<td><c:url value="/cards/list" var="cList" /> 
				<a href="${cList}"> All cards list</a><br/>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<td> <c:url value="/cards/newCard" var="nCard" />
			<a href="${nCard}">New Card</a><br/>
			<td><c:url value="/hello" var="hello" /> 
				<a href="${hello}">hello</a><br/>
		</tr>
	</table>
	<br/><br/>
		<cf:form method="GET" action="/CardB/cards/search.form" commandName="/cards/search.form" >		SEARCH
		<input  name="str" id="str" /> <!-- rows="4" cols="50" -->
		 
	     	 <input type="submit" >			
		</cf:form>
	<br/>
	
	
	<table>
		<tr>
			<th></th>
			<th>Id</th>
			<th>category</th>
			<th>overview</th>
			<th>createDate</th>			
		</tr>
		<c:forEach var="cat" items="${catList}">
			<tr>
				<td><a href="${cat.id}" title="CARDS">cards</a></td>
				<td><c:out value="${cat.id}" />  </td>				
				<td><c:out value="${cat.category}" /></td>
				<td><c:out value="${cat.overview}" /></td>
				<td><c:out value="${cat.createDate}" /></td>
				
							
			</tr>			
		</c:forEach>
				<tr>
		 			 <td colspan="5"><c:url value="/cards/cardsNoCat" var="noCat" /> 
						<a href="${noCat}">Cards without category</a><br/></td>
				</tr>	
	</table>
	
			
	

	
	
	
	
	</div>
		
		
		
		
		
		
		
	</body>
</html>
	