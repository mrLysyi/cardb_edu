 CREATE TABLE `t_card` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `overview` text,
  `text` longtext NOT NULL,
  `searchTeg` mediumtext NOT NULL,
  `category` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `updateDate` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `createDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8;
--------------------------------------




insert into merchant (name, charge, period, minSum, bankName, swift, account) values('Jim Smith Ltd.', 5.1, 1, 100.0, 'Chase Manhatten', 'AA245BXW', '247991002');
insert into merchant (name, charge, period, minSum, bankName, swift, account) values('Domby and sun Co.', 2.8, 2, 20.0, 'Paribas', 'XTW2NNM', '1188532009');
insert into merchant (name, charge, period, minSum, bankName, swift, account) values('Victoria Shop Ltd.', 3.4, 3, 500.0, 'Swedbank', 'SWEE34YY', '557880234');
insert into merchant (name, charge, period, minSum, bankName, swift, account) values('Software & Digital goods', 4.9, 1, 160.0, 'Credi Leone', 'FRTOPM', '367920489');
//-------------------------------------------------------------
create schema base;
set schema base;

drop table base.card;
base.cardToCategory
base.category
drop table base.categoryType;
drop table base.usergroupT;
drop table base.userT;
drop table base.card;
DROP SCHEMA BASE RESTRICT;

//-------------------------------------------
ALTER table base.card
DROP  CONSTRAINT status_fk;
drop table base.status;

ALTER table base.cardTocategory DROP  CONSTRAINT typet_fk;
ALTER table base.cardTocategory DROP  CONSTRAINT category_fk;
ALTER table base.cardTocategory DROP  CONSTRAINT card_fk;
ALTER table base.categoryToType DROP  CONSTRAINT categoryType_fk;
ALTER table base.categoryToType DROP  CONSTRAINT typeT_fk;
drop table base.card;
drop table base.typeT;
drop table base.category;
drop table base.cardToCategory;
drop table base.categoryToType;
drop table base.usergroupT;
drop table base.userT;

//--------------------
//jdbc:derby:/home/username/Public/java/MyDB_derby/;create=true

//----------------------------------------------------------------
select all card by specified category:
select * from Card c
		INNER JOIN CardToCategory ctc on c.id = ctc.cardId		
		INNER JOIN category cat on ctc.categoryId = cat.id
		where cat.category='j_h';
