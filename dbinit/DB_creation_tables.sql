 
CREATE TABLE status (
id int not null generated always as identity,	
name varchar(60) not null UNIQUE,
primary key (id)
);

CREATE TABLE card (
id int not null generated always as identity,	
name varchar(60) not null UNIQUE,
overview varchar(1000) not null,
searchTeg varchar(1000),
createDate date not null WITH DEFAULT CURRENT DATE,
updateDate date not null,
text LONG VARCHAR,
statusId int constraint status_fk references status,
 primary key (id)
) ;

CREATE TABLE typeT(
id int not null generated always as identity,	
name varchar(60) not null UNIQUE,
overview varchar(1000) not null,
 primary key (id)
);

CREATE TABLE category (
id int not null generated always as identity,	
category varchar(100) not null UNIQUE,
overview varchar(1000) not null,
createDate date not null WITH DEFAULT CURRENT DATE,
 primary key (id)
);


CREATE TABLE cardToCategory (
id int not null generated always as identity,	
cardId int constraint card_fk references card,
categoryId int constraint category_fk references category,
 primary key (id)
);

CREATE TABLE categoryToType(
id int not null generated always as identity,
categoryId int constraint categoryType_fk references category,
typeTId int constraint typeT_fk references typeT,
primary key (id)
);


CREATE TABLE userT(
id int not null generated always as identity,	
username varchar(45) not null UNIQUE,
password varchar(64) not null,
email varchar(320) not null UNIQUE,
 primary key (id)
);



CREATE TABLE usergroupT(
id int not null generated always as identity,
username varchar(45) NOT NULL UNIQUE,
groupname varchar(45) NOT NULL,
 primary key (id)
);
