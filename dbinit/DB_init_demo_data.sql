
insert into status (name) values ('new');
insert into status (name) values ('active');
insert into status (name) values ('done');
insert into status (name) values ('deleted');


insert into card (name, overview, searchTeg, updateDate, text, statusId) values('javadock','dock overview', 'java docks oracle', '2016-12-20', 'text ref to javadocks ect text', 1);
insert into card (name, overview, searchTeg, updateDate, text, statusId) values('jdk', 'jdk overview','java jdk virtual machine oracle', '2016-12-21', 'text REF jdk ect', 1);
insert into card (name, overview, searchTeg, updateDate, text, statusId) values('spring', 'spring overview','java docks spring', '2016-12-23' ,'text ref to SPRING spring... тест спринг',2);

insert into category (category, overview) values('java','java category');
insert into category (category, overview) values('j_h','hard topics about java');
insert into category (category, overview) values('spring','spring EE framework');


insert into cardToCategory(cardId, categoryId ) values (1,1);
insert into cardToCategory(cardId, categoryId ) values (1,2);
insert into cardToCategory(cardId, categoryId ) values (2,2);
insert into cardToCategory(cardId, categoryId ) values (3,3);


insert into typeT(name, overview  ) values ('common','different information');
insert into typeT(name, overview  ) values ('java', 'джава');

insert into categoryToType(categoryId, typeTId ) values (1,1);
insert into categoryToType(categoryId, typeTId ) values (1,2);
insert into categoryToType(categoryId, typeTId ) values (2,2);
insert into categoryToType(categoryId, typeTId ) values (3,2);



INSERT INTO usergroupT  (username, groupname) VALUES ('John Doe','admin');
INSERT INTO usergroupT  (username, groupname) VALUES ('admin','admin');
INSERT INTO usergroupT  (username, groupname) VALUES ('javaUser','java');
INSERT INTO usergroupT  (username, groupname) VALUES ('otherUser','other');


insert into usert (username, password, email) values ('John Doe','9F86D081884C7D659A2FEAA0C55AD015A3BF4F1B2B0B822CD15D6C15B0F00A08', 'test@pmail.com');
insert into usert (username, password, email) values ('admin','111', 'admin@tmail.su');
